const express = require('express');
const router = express.Router();
const googleTaskServices = require('./../http/services/googleTaskServices');

router.get('/', function(req, res, next) {
   (new googleTaskServices).loadCredential(req.query, tasks => {
       res.json(tasks);
   });
});

module.exports = router;
