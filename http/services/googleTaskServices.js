"use strict";

const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const request = require('request');


class googleTaskServices{

    constructor(){}

    loadCredential(params, cback) {
        const oAuth2Client = new google.auth.OAuth2(params);
        oAuth2Client.setCredentials(params);
        this.getTaskList(oAuth2Client,cback);
    }

    getTaskList(auth, cback){
        const service = google.tasks({version: 'v1', auth});
        const tasksArr = {};
        service.tasklists.list({
            maxResults: 10
        }, (err, res) => {
            if (err){
                return console.error('The API returned an error: ' + err);
            }
            this.waitForeach(service, tasksArr, res.data.items, 0, cback);
        });
    }

    waitForeach(service, tasksArr, taskList , index, cback){
        if(index < taskList.length && taskList[index]){
            tasksArr[taskList[index].id] = taskList[index];
            service.tasks.list({tasklist:taskList[index].id},(err, res) => {
                if(err){
                    console.log({'error':'server error'});
                }
                tasksArr[taskList[index].id]['subItem'] = res.data.items;
                index = (index+1);
                this.waitForeach(service, tasksArr, taskList , index ,cback);
            });
        }else{
            cback(tasksArr);
        }
    }
};

module.exports = googleTaskServices;